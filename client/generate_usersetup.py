# -*- coding: utf-8 -*-


import os
import shutil
import sys
import argparse


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dont-add-to-maya', '-d', default=False, 
                        action='store_true')
    namespace = parser.parse_args()
    return namespace


if __name__ == '__main__':
    args = parse_args()

    cur_dir = os.path.dirname(os.path.abspath(__file__))
    loader_dir = os.path.dirname(cur_dir)
    load_mel = os.path.join(loader_dir, 'load.mel').replace('\\', '/')

    output_path = os.path.join(cur_dir, 'userSetup.mel')
    input_path = os.path.join(cur_dir, 'userSetup.mel.in')

    with open(input_path) as input_file:
        data = input_file.read()

    data = data.format(**{'PATH_TO_LOAD_MEL': load_mel})

    try:
        with open(output_path, 'w') as output_file:
            output_file.write(data)
        print('file written to {}'.format(output_path))

    except os.PermissionError:
        print('failed to write to file {}'.format(output_path))

    maya_dir = os.environ.get('MAYA_APP_DIR')
    if not args.dont_add_to_maya and maya_dir:
        userSetup = os.path.join(maya_dir, 'userSetup.mel')
        if not os.path.isfile(userSetup):
            shutil.copy(output_path)
        else:
            with open(userSetup, 'a') as _file:
                _file.write(data)
